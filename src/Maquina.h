/**
 ******************************************************************************
 * @file     Maquina.h
 * @author  Sergio Alberino
 * @version 0.1
 * @date    27/06/2017
 * @brief   Funciones para el control de Motores
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */
 
 
#ifndef _MDE_H_
#define _MDE_H_

#include <LPC17xx.h>

#define DRIVER_IR 	1
#define US 			1
#define PULSADOR 	1		
#define AS5045		1

///**
// * @brief Inicializa Máquina de estados Principal
// * @details Inicializa todos los servicios relacionados con la Máquina de estados Principal
// */
//void Maquina_Init(void);
void MdETsk (void *params);

#endif
