/**
 ******************************************************************************
 * @file        main.c
 * @author  		ale
 * @version
 * @date
 * @brief
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#include <main.h>
#include <stdlib.h>
//#include <stdio_USART.h>
//#include <stdio.h>
#include <cmsis_os2.h>
#include <Maquina.h>


//-------- <<< Use Configuration Wizard in Context Menu >>> --------------------
 
// <e> RUN Tests
#define RUN_TESTS 0
#if RUN_TESTS

//   <e>Enable AS5045 Test<0-1>
    #define TEST_AS5045 0
	#if TEST_AS5045
		#include "test_as5045.c"
	#endif
	
// </e>

//   <e>Enable Printf Test<0-1>
#define TEST_PRINTF 0
	#ifdef TEST_PRINTF

		#include "test_printf.c"
	#endif
// </e>	

//   <e>Enable Leds Test<0-1>
#define TEST_LEDS 0
	#ifdef TEST_LEDS
		#include "test_Leds.c"
	#endif
//   <e>Enable IR Line Test<0-1>
    #define TEST_IR_LINE 0
	#if TEST_IR_LINE
		#include "test_ir.c"
	#endif
// </e>

//   <e>Enable SR04 Test<0-1>
    #define TEST_US 0
	#if TEST_US
		#include "test_sr04.c"
	#endif
// </e>

//   <e>Enable Pulsadores Test<0-1>
    #define TEST_PULSADORES 0
	#if TEST_PULSADORES
		#include "test_Pulsadores.c"
	#endif
// </e>	


//   <e>Enable Motor Test<0-1>
    #define TEST_MOTOR 0
	#if TEST_MOTOR
		#include "test_Motor.c"
	#endif
// </e>	

#endif

// </e>
 
#ifndef __assert_h
void __aeabi_assert(const char *expr, const char *file, int line)
{
	abort();
}
#endif


int main (void)
{
//    stdio_init();

//    LPC_WDT->WDMOD = 0;
    
    osKernelInitialize();
    
		#if RUN_TESTS
			#if TEST_AS5045
				osThreadNew( test_as5045_task, NULL, &test_as5045_task_attr);
			#endif
            #if TEST_MOTORDC
                test_Motor();
            #endif
			#if TEST_PRINTF
				test_printf();
			#endif
            #if TEST_LEDS
				test_leds();
			#endif
            #if TEST_IR_LINE
                test_ir();
			#endif
			#if TEST_US
				test_sr04();
		    #endif
			#if TEST_PULSADORES
				test_pulsador();
		    #endif
            #if TEST_MOTOR
				test_Motor();
		    #endif
		#endif
        
        
    // Create Switch Task
    const osThreadAttr_t attributes = {.stack_size = 2048};
    osThreadNew(MdETsk, NULL, &attributes);
    
    osKernelStart();
    
    while(1){
        /* You should not enter here. */
    };
}
