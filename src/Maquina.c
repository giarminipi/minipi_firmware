/**
 ******************************************************************************
 * @file    Maquina.c
 * @author  Sergio Alberino
 * @version 0.1
 * @date    27/06/2017
 * @brief   Rutinas de la Máquina de Estados Principal
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#include <Maquina.h>
#include <MdE.h>
#include <cmsis_os2.h>
#include <stdbool.h>
#include <stdlib.h>

#include <Leds.h>
#include "Mid_Motores.h"
#include "Mid_IR.h"
#include "Mid_Pulsadores.h"
#include "Mid_US.h"
#include "Mid_AS5045.h"


uint8_t global_input1 = NULL, global_input2 = NULL;
extern osThreadId_t mde_tsk_id;
osEventFlagsId_t read_data_event;
const osThreadAttr_t test_as5045_task_attr = {
  .stack_size = 1024    // Create the thread stack with a size of 1024 bytes
};

/**
 * @brief Inicializa Máquina de Estados Principal
 * @details Inicializa todos los servicios relacionados con la Máquina de estados Principal
 */
void MdETsk (void *params){
	static Led *led1,*led2,*led3,*led4;
    static Led led01,led02,led03,led04;
    
    led1=&led01;
    led2=&led02;
    led3=&led03;
    led4=&led04;
    // Get Thread ID
    mde_tsk_id = osThreadGetId();
    
    // Init Leds
    initLed(&led1,1,LED_1,1000,1000,100);//!Led_1
    initLed(&led2,2,LED_2,0,500,10);//!Led_2
    initLed(&led3,3,LED_3,2000,1000,100);//!Led_3
    initLed(&led4,4,LED_4,0,1500,10);//!Led_4
    
    // Generic Thread Atrributes
    osThreadAttr_t attributes = {
        .stack_size = 128,
        .priority = osPriorityISR,
    };
    
    // Create Switch Task
    osThreadId_t pTsk = osThreadNew(pulsadorTsk, (void*) &pp, &attributes);
    
    // Wait for switch
    if( osThreadFlagsWait(FLAG_SWITCH_RELEASE, osFlagsWaitAll, osWaitForever) != FLAG_SWITCH_RELEASE )
        while(1){
            led1->ledOff(led1);
            osDelay(500);
            led1->ledOn(led1);
            osDelay(500);
        }
    
    // Delete Pulsador Task
    osThreadTerminate(pTsk);
            
    // LEDs secuence
    led1->ledOn(led1);
    osDelay(300);
    led1->ledOff(led1);
    led2->ledOn(led2);
    osDelay(300);
    led2->ledOff(led2);
    led4->ledOn(led4);
    osDelay(300);
    led4->ledOff(led4);
    led3->ledOn(led3);
    osDelay(300);
    led3->ledOff(led3);

            
    // Create IR Line Sensor Tasks
    osThreadNew(irTsk, (void*) &ir_1, &attributes);
    osThreadNew(irTsk, (void*) &ir_2, &attributes);
    osThreadNew(irTsk, (void*) &ir_3, &attributes);
    osThreadNew(irTsk, (void*) &ir_4, &attributes);
    
    // Create IR Line Sensor Tasks
    attributes.stack_size = 1024;
    osThreadNew(SR04Task, (void*) &us1_data, &attributes);
    osThreadNew(SR04Task, (void*) &us2_data, &attributes);
    osThreadNew(SR04Task, (void*) &us3_data, &attributes);
    osThreadNew(SR04Task, (void*) &us4_data, &attributes);

    // Init Motors and Enable Motors and leave them to 0 speed
	Motor_PWM_Init(MOTOR_FREQ_HZ);
    Motor_SetSpeed(MOTOR1, 0);
    Motor_SetSpeed(MOTOR2, 0);
	Motor_Cmd(MOTOR1, M_ENABLE);                            // Se Habilita Motor1
	Motor_Cmd(MOTOR2, M_ENABLE);                            // Se Habilita Motor2

    uint8_t state = NULL, input1 = NULL, input2 = NULL;

    for(;;){

        input1 = global_input1;
        input2 = global_input2;
        
        // *************** Rutina MdE. recorre la tabla, hasta que encuentra un Estado FF *****************
        for (uint32_t i=0; Planilla[i].state != 0xFF; i++)
        {
            // coincide la entrada con la combinación de la Planilla?
            if (Planilla[i].state == state && Planilla[i].input1 == input1 && Planilla[i].input2 == input2)
            {
                SEL_COM (Planilla[i].com, Planilla[i].parameters);
                state      = Planilla[i].future_state;
                break; // sale del while porque encontró el ESTADO y la condición coincidente en la TABLA
            }
        }
                
        // Espero 500mseg
        osDelay(500);
    }
}
