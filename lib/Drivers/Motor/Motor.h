/**
  ******************************************************************************
  * @file    Motor.h
  * @author  Sergio Alberino
  * @version 1.1
  * @date    27/06/2017
  * @brief   Funciones para el control de Motores
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/


#include <LPC17xx.h>
#include <PIN_LPC17xx.h>
#include <GPIO_LPC17xx.h>

#include <lpc17xx_pwm.h>          // PWM

#define PORT_1             1
#define PORT_2             2
#define PORT_MOTOR_1       PORT_2
#define PORT_MOTOR_2       PORT_2
#define PIN_MOTOR_1        0
#define PIN_MOTOR_2        1	
#define PORT_HAB_1         PORT_1
#define PORT_HAB_2         PORT_1
#define PIN_HAB_1          22
#define PIN_HAB_2          25	
#define PWMChannel_MOTOR_1 1
#define PWMChannel_MOTOR_2 2	
#define PWMMatch_MOTOR_1   1	
#define PWMMatch_MOTOR_2   2	


/** PWMs
 */
typedef enum
{
	PWM1 = 1,
	PWM2,
}PWMx;

/** MOTORes
 */
typedef enum
{
	MOTOR1 = 	1,
    MOTOR2,      
}MOTORx;

/** ESTADOS */
typedef enum
{
    M_ENABLE  = 0,        // El motor se habilita con 0
    M_DISABLE,
}ESTADOx;


/**
 * @brief Habilita el Puente "H" para MotorX
 * @details Habilita el PIN correspondiente para el Puente "H" para MotorX
 * @param motor: puede ser MOTOR1 o MOTOR2
 * @param estado: Habilitado M_ENABLE o Deshabilitado M_DISABLE
 */
void Motor_Cmd(MOTORx motor, ESTADOx estado);

/**
 * @brief Inicializa PWM para ambos motores
 * @details Inicializa PINES y configuración PWM para ambos motores
 * @param frecuencia: Frecuencia del PWM [KHz]
 */
//void Motor_PWM_Init(float pwm_tick, float pwm_freq, int PRESCALER);
void Motor_PWM_Init(uint32_t frecuencia);


/**
 * @brief Actualiza el Duty Cicle de MOTORx
 * @details Actualiza valor Ciclo de actividad para el PWM de MOTORx
 * @param motor: puede ser MOTOR1 o MOTOR2
 * @param duty: Ciclo de actividad para el PWM de MOTORx
 * Note: Para setear la velocidad usar Motor_SetSpeed
 */
void Motor_DutyUpdate(MOTORx motor, uint32_t duty);

/**
 * @brief Actualiza la velocidad de MOTORx
 * @details Actualiza valor Ciclo de actividad para el PWM de MOTORx
 * @param motor: puede ser MOTOR1 o MOTOR2
 * @param speed: Porcentaje signado de la velocidad máxima del motor
 */
void Motor_SetSpeed(MOTORx motor, int speed);


/**
 * @brief Función de interrupción 
 * @details Esta función debe ser sobreescrita por el usuario
 */
__weak void irqPWM (void);	
