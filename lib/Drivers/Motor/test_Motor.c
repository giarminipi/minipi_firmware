/**
  ******************************************************************************
  * @file    	test_Motor.c
  * @author		Alejandro Alvarez
  * @version	1.0
  * @date		02/10/2018
  * @brief		Programa de testeo para motores de continua
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
	
#ifdef TEST_MOTORDC
#include <Motor.h>
#include <Leds.h>
#include <cmsis_os2.h>
#include <stdlib.h>


#define MOTOR_FREQ_HZ 20000

static uint16_t led_delay = 100;

const osThreadAttr_t Motor_task_att = {
    .name = "Motor Task",
    .stack_size = 128
}; 

const osThreadAttr_t LED_task_att = {
    .name = "LED Task",
    .stack_size = 128
};  

void LED_task (void *argument)
{
    for(;;)
    {
        ledToggle(LED_1);
        osDelay(led_delay);
    }
}

void Motor_task (void *argument)
{
	Motor_PWM_Init(MOTOR_FREQ_HZ);                          // Inicializa PWM y setea la frecuencia en Hz
	Motor_Cmd(MOTOR1, M_ENABLE);                            // Se Habilita Motor1
	Motor_Cmd(MOTOR2, M_ENABLE);                            // Se Habilita Motor2
	initLed();                                              //	Inicializa LEDs

    const uint8_t NUM_STEPS = 10;
    int8_t count = NUM_STEPS;
    
	for(;;) {
        
        if(++count > NUM_STEPS)
            count = (-1)*NUM_STEPS;
        
        // Update motor speed
        Motor_SetSpeed(MOTOR1, count*NUM_STEPS);                       // Setea nuevo Duty para PWM
        Motor_SetSpeed(MOTOR2, count*NUM_STEPS);                       // Setea nuevo Duty para PWM
        
        // Update Led delay
        led_delay = 1000/abs(count);
        
        // Wait 5 seconds
        osDelay(5000);
	}
}


void test_Motor(void){
    // Create task
    osThreadNew(Motor_task, NULL, &Motor_task_att);
    osThreadNew(LED_task, NULL, &LED_task_att);
}

#endif
