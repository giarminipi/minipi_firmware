/**
  ******************************************************************************
  * @file    Motor.c
  * @author  Sergio Alberino
  * @version 1.1
  * @date    17/07/2017
  * @brief   Funciones para el control de Motores
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#include <Motor.h>
#include <lpc17xx_clkpwr.h>
#include <stdlib.h>

#define SIGN(x) ((int8_t)((x>0) ? 1:-1))

void Motor_Cmd(MOTORx motor, ESTADOx estado)
{
    switch(motor) {
        case MOTOR1:
            GPIO_SetDir (PORT_HAB_1, PIN_HAB_1, GPIO_DIR_OUTPUT);
            GPIO_PinWrite (PORT_HAB_1, PIN_HAB_1, estado);
            break;
        case MOTOR2:
            GPIO_SetDir (PORT_HAB_2, PIN_HAB_2, GPIO_DIR_OUTPUT);
            GPIO_PinWrite (PORT_HAB_2, PIN_HAB_2, estado);
            break;
    }
}

void Motor_PWM_Init( uint32_t frecuencia)
{
    /* Apaga contador */
    PWM_CounterCmd(LPC_PWM1, DISABLE);

    /* Declara estructuras para configuración */
    PWM_TIMERCFG_Type PWMCfgDat;
    PWM_MATCHCFG_Type PWMMatchCfgDat;    

    uint32_t PCLK_PWM = (uint32_t) CLKPWR_GetPCLK (CLKPWR_PCLKSEL_PWM1);
    uint32_t Match_value = PCLK_PWM / frecuencia;

    /* Inicializa los PINes de PWM para el Motor 1 y 2*/
    PIN_Configure (PORT_MOTOR_1, PIN_MOTOR_1, PIN_FUNC_1, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);
    PIN_Configure (PORT_MOTOR_2, PIN_MOTOR_2, PIN_FUNC_1, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);

    /* Inicializa el PWM en timer mode y prescale */
    PWMCfgDat.PrescaleOption = PWM_TIMER_PRESCALE_TICKVAL;
    //TODO: crear lógica para completar el prescaler
    PWMCfgDat.PrescaleValue = 1;
    PWM_Init(LPC_PWM1, PWM_MODE_TIMER, (void *) &PWMCfgDat);

    /* Setea frecuencia PWM */
    PWM_MatchUpdate(LPC_PWM1, 0, Match_value, PWM_MATCH_UPDATE_NOW);
    PWMMatchCfgDat.IntOnMatch = DISABLE;
    PWMMatchCfgDat.MatchChannel = 0;
    PWMMatchCfgDat.ResetOnMatch = ENABLE;
    PWMMatchCfgDat.StopOnMatch = DISABLE;
    PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);

    /* Setea valor PWM1 50% */
    PWM_MatchUpdate(LPC_PWM1, PWMMatch_MOTOR_1, Match_value/2, PWM_MATCH_UPDATE_NOW);
    PWMMatchCfgDat.IntOnMatch = DISABLE;
    PWMMatchCfgDat.MatchChannel = PWMChannel_MOTOR_1;
    PWMMatchCfgDat.ResetOnMatch = DISABLE;
    PWMMatchCfgDat.StopOnMatch = DISABLE;
    PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);

    /* Setea valor PWM2 50%  */
    PWM_MatchUpdate(LPC_PWM1, PWMMatch_MOTOR_2, Match_value/2, PWM_MATCH_UPDATE_NOW);
    PWMMatchCfgDat.IntOnMatch = DISABLE;
    PWMMatchCfgDat.MatchChannel = PWMChannel_MOTOR_2;
    PWMMatchCfgDat.ResetOnMatch = DISABLE;
    PWMMatchCfgDat.StopOnMatch = DISABLE;
    PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);

    /* Resetea contador */
    PWM_ResetCounter(LPC_PWM1);

    /* Arranca contador */
    PWM_CounterCmd(LPC_PWM1, ENABLE);

    /* Configura Channel PWM */
    PWM_ChannelCmd(LPC_PWM1, PWMChannel_MOTOR_1, ENABLE);        // setea PCR, PWMENAble para PWM1
    PWM_ChannelCmd(LPC_PWM1, PWMChannel_MOTOR_2, ENABLE);        // setea PCR, PWMENAble para PWM2

    /* Arranca PWM */
    PWM_Cmd(LPC_PWM1, ENABLE);

//    /* Habilita interrupcion PWM */
//    NVIC_EnableIRQ(PWM1_IRQn);

}

static void Motor_DutyUpdate(MOTORx motor, uint32_t Duty)
{
    if (Duty > 100)    // se limita el valor de Duty
        Duty=100;

    /* Actualiza Valor PWM  */
    PWM_MatchUpdate(LPC_PWM1, motor, LPC_PWM1->MR0 * Duty / 100, PWM_MATCH_UPDATE_NOW);    
}

void Motor_SetSpeed(MOTORx motor, int speed)
{
    if( abs(speed) > 100)
        speed = 100 * SIGN(speed) ;

    speed += 100;

    Motor_DutyUpdate(motor, (uint32_t) speed/2);
}
