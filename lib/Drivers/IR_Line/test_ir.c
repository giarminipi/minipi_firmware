/**
 ******************************************************************************
 * @file			test_Pulsadores.c
 * @author	    	Alejandro Gastón Alvarez
 * @version	    	0.1
 * @date			05/11/2018
 * @brief			Programa de testeo de Pulsadores
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#include <ir_line.h>
#include <cmsis_os2.h>
#include <stdlib.h>
#include <Leds.h>

void ir_1_event (void){
    ledToggle(LED_1);
}

void ir_2_event (void){
    ledToggle(LED_2);
}

void ir_3_event (void){
    ledToggle(LED_3);
}

void ir_4_event (void){
    ledToggle(LED_4);
}

const IR_Params ir_1 = {
    .port = 0,
    .pin = 23,
    .onRise = ir_1_event,
    .onFalling = ir_1_event,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_2 = {
    .port = 0,
    .pin = 24,
    .onRise = ir_2_event,
    .onFalling = ir_2_event,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_3 = {
    .port = 0,
    .pin = 25,
    .onRise = ir_3_event,
    .onFalling = ir_3_event,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_4 = {
    .port = 0,
    .pin = 26,
    .onRise = ir_4_event,
    .onFalling = ir_4_event,
    .sampling_time = 1,
    .min_samples = 20
};


void test_ir(void){
    initLed();
    
    const osThreadAttr_t attributes = { 
        .stack_size = 128
    };
    
    osThreadNew(irTsk, (void*) &ir_1, &attributes);
    osThreadNew(irTsk, (void*) &ir_2, &attributes);
    osThreadNew(irTsk, (void*) &ir_3, &attributes);
    osThreadNew(irTsk, (void*) &ir_4, &attributes);
}
