/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#include <ir_line.h>
#include <cmsis_os2.h>
#include <GPIO_LPC17XX.h>

static void update_integrator(uint8_t* integrator, const uint8_t current_read, const uint8_t min_samples ){
    if( current_read ) {
        if( *integrator < min_samples )
            (*integrator)++;
    } else {
        if( *integrator > 0 )
            (*integrator)--;
    }
}

static Ir_State debounce_logic (uint8_t* integrator,  const Ir_State state, const uint8_t current_read, const uint8_t min_samples, uint8_t *counter) {
    // Update integrator with current read
    update_integrator(integrator, current_read, min_samples);

    // Do maximum times while
    if ( *counter < min_samples - 1){
        (*counter)++;
    }
    else {
        // Check if I'm outside the window
        if ( ! (*integrator < min_samples && *integrator > 0 ) )
            return *integrator ? IR_WHITE : IR_BLACK;
    }
    return IR_DEBOUNCING;
}

void irTsk (void *params){

    IR_Params* ir = (IR_Params*) params;
    
    uint8_t integrator = ir->min_samples;
    uint8_t counter = 0;
    Ir_State state = IR_BLACK;

    GPIO_SetDir(ir->port, ir->pin, GPIO_DIR_INPUT);
    
    for(;;){
        // Read pin
        Ir_State current_read = (Ir_State) GPIO_PinRead (ir->port, ir->pin);
        
        // If in debouncing ==> counter != 0 or if current state is different from state
        if( counter || current_read != state) {
            
            // Run debounce logic
            Ir_State new_state = debounce_logic(&integrator, state, current_read, ir->min_samples, &counter);
        
            switch(new_state){
                case IR_BLACK:
                    integrator = 0;
                    counter = 0;
                    if (state != new_state)
                        ir->onFalling();
                    state = new_state;
                    break;
                case IR_WHITE:
                    integrator = ir->sampling_time;
                    counter = 0;
                    if (state != new_state)
                        ir->onRise();
                    state = new_state;
                    break;
                case IR_DEBOUNCING:
                default:
                    break;
            }
        }
        
        // Sleep the requiered time
        osDelay(ir->sampling_time * (osKernelGetTickFreq()/1000) );
    }
}

