/**
  ******************************************************************************
  * @file    in_line.h
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _IR_SENSOR_H_
#define _IR_SENSOR_H_

#include <stdlib.h>
#include <stdint.h>

typedef struct {
    uint8_t port;
    uint8_t pin;
    uint32_t sampling_time;
    uint8_t min_samples;
    void (*onRise) ( void );
    void (*onFalling) ( void );
}IR_Params;
    
typedef enum {
    IR_DEBOUNCING = -1,
    IR_WHITE = 0,
    IR_BLACK = 1,
}Ir_State;
    
void irTsk (void *parameters);
    
    
#endif    // End _IR_SENSOR_H_
