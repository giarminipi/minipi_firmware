/**
  ******************************************************************************
  * @file    led.c
  * @author  Sergio Alberino
  * @version 1.1
  * @date    18/07/2017
  * @brief   Funciones para el control de LEDs
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
    */

#include "Leds.h"
#include <GPIO_LPC17XX.h>

#define IN     0
#define OUT    1
#define OFF 	 0
#define ON 		 1
#define PORT_0 0
#define PORT_1 1
#define T_INTENSIDAD 40 //!Periodo maximo de Duty Cicle para el cambio de intensidad

Led* F_leds[4]={0,0,0,0};//!Vector de punteros a Led

/**
 * @brief Encendido de LED.
 * @details Todos los LEDs estan asociados al Puerto 1.
 * @param [in,out] led LED que se quiere encender.
 */
void ledOn_ ( Led *led )
{
  GPIO_PinWrite(PORT_1,led->Pin,ON);//!Enciende el led, escribendo en el pin 
}

/**
 * @brief Apagado de LED.
 * @details Todos los LEDs estan asociados al Puerto 1.
 * @param [in,out] led LED que se quiere apagar.
 */
void ledOff_ ( Led *led )
{
  GPIO_PinWrite(PORT_1,led->Pin,OFF);//!Apaga el led, escribiendo en el pin 
}

/**
 * @brief Inversion de estado de LED.
 * @details Todos los LEDs estan asociados al Puerto 1.
 * @param [in,out] led LED al que se le quiere invertir el estado.
 */
void ledToggle_ ( Led *led )
{
	GPIO_PinWrite(PORT_1,led->Pin, !GPIO_PinRead (PORT_1,led->Pin) );//!Cambia el estado, escribiendo en el pin del led
}
/**
 * @brief Regula intensidad leds.
 * @details Mediante el timer.
 * @param [in,out] led LED al cual se le quiere regular la intensidad.
 */
void Intensidad_ ( Led *led )
{

    if( ( 100 - led->Intensidad_Porcentaje ) <  led->Cont_Intensidad )
    {
        led->Cont_Intensidad = led->Cont_Intensidad - 5;//!decremento el contador de intensidad, osea cada 5%
        if( led->F_on_off == 0 )//!enciende una sola vez 
        {
            led->ledOn( led );//!enciende el led
            led->F_on_off=1;//!asigno el flag para apagar
        }
    }
    else
    {
        led->Cont_Intensidad = led->Cont_Intensidad - 5;
        if( led->F_on_off == 1)//!apaga una sola vez
        {
            led->ledOff( led );//!apaga el led
            led->F_on_off=0;//!reinicio flag de apagado/prendido de led
        }
        if ( led->Cont_Intensidad <= 0 )
        {
            led->Cont_Intensidad = 100;//!reinicia el contador de intensidad
        }
    }
}

/**
 * @brief Hacer que el led parpadee.
 * @details Mediante la utilizacion del timer hago que el led se prenda y apage durante cierto tiempo.
 * @param [in,out] led LED el cual se quiere hacer parpadear.
 */
void Parpadear_ ( Led *led )
{
    if ( led->F_Tiempo == 1 )
	{	
		led->F_Tiempo = 0;//reinicio flag del timer del led
        if( led->Cont_Prendido < led->Tiempo_Prendido )
        {
            (led->Cont_Prendido)++;//!incrementa el contador de tiempo prendido del led
            led->Intensidad( led );//!prende el led desde la funcion Intensidad()
        }
        if( (led->Cont_Prendido) == led->Tiempo_Prendido )
        {
            (led->Cont_Apagado)++;//!incrementa contador de tiempo apagado
            if( led->F_off == 0 && led->Tiempo_Apagado!= 0)//!entra una sola vez a apagar el led
            {
                led->ledOff( led );//!apago el led
                led->F_off=1;
            }
            if( led->Cont_Apagado == led->Tiempo_Apagado || led->Tiempo_Apagado == 0 )
            {
                led->Cont_Apagado =  0;//!reinicio contador de tiempo apagado del led
                led->Cont_Prendido = 0;//!reinicio contador de tiempo prendido del led
                led->F_off=0;//!reinicio flag de apagar del led
            }
        }
    }
}

/**
 * @brief Setea el valor de intensidad.
 * @details Funcion que setea el valor de porcentaje con el que brilla el led.
 * @param [in,out] led LED el cual se quiere modificar.
 * @param [in]Intensidad_Porcentaje Porcentaje al que se le va a setear el LED.
 * @return Devuelve 0 si se pudo setear, 1 si hay error.
 */
uint8_t Set_Intensidad_Porcentaje_ ( Led **led, uint8_t Intensidad_Porcentaje )
{
    if( Intensidad_Porcentaje > 100 )//! evita que ingrese un valor ilogico
        return 1;
    else
        ( *led )->Intensidad_Porcentaje = Intensidad_Porcentaje;//!asigna la intensidad del led
    return OK;
}

/**
 * @brief Setea el valor del tiempo prendido.
 * @details Funcion que setea el valor del tiempo que se mantiene prendido el led.
 * @param [in,out] led LED el cual se quiere modificar.
 * @param [in] Tiempo_Prendido Valor del tiempo que el LED va estar prendido.
 * @return Devuelve 0 si se pudo setear, 1 si hay error.
 */
uint8_t Set_Tiempo_Prendido_ ( Led **led, uint32_t Tiempo_Prendido )
{
	( *led )->Tiempo_Prendido = Tiempo_Prendido;//!asigna el tiempo prendido
	return OK;
}

/**
 * @brief Setea el valor del tiempo apagado.
 * @details Funcion que setea el valor del tiempo que se mantiene apagado el led.
 * @param [in,out] led LED el cual se quiere modificar.
 * @param [in] Tiempo_Apagado Valor del tiempo que el LED va a estar apagado. 
 * @return Devuelve 0 si se pudo setear, 1 si hay error.
 */
uint8_t Set_Tiempo_Apagado_ ( Led ** led, uint32_t Tiempo_Apagado )
{
	( *led )->Tiempo_Apagado = Tiempo_Apagado;//!asigna el tiempo apagado
	return OK;
}

/**
 * @brief Setea el valor del tiempo en que cambia de estado.
 * @details Funcion que setea el valor del tiempo total del parpadeo, osea suma los valores de tiempo prendido y apagado.
 * @param [in,out] led LED el cual se quiere modificar.
 * @return Devuelve 0 si se pudo setear, 1 si hay error.
 */
uint8_t Set_Tiempo_Parpadear_ ( Led **led)
{
		if( ((*led)->Tiempo_Apagado + (*led)->Tiempo_Prendido) > T_INTENSIDAD*2 )//! mayor a 2 duty cicle maximo de intensidad
			( *led )->Tiempo_Parpadear = (*led)->Tiempo_Apagado + (*led)->Tiempo_Prendido;
		else
			return ERRORS;
		return OK;
}

/**
 * @brief Inicializacion de LEDs.
 * @details Definicion y configuracion de cada LED.
 * @param [in] led ,puntero a led a modificar.
 * @param [in] pin ,pin de LED que va a ser inicializado.
 * @param [in] numero ,numero de led a ser cargado
 * @param [in] t_apagado ,tiempo apagado del parpadeo a ser cargado
 * @param [in] t_prendido ,tiempo prendido del parpadeo a ser cargado
 * @param [in] intensidad ,porcentaje de intensidad del led
 * @return Devuelve 1 por exito,devuelve 0 si hubo error
 * @note Esta funcion pone el puerto en 1
*/
uint8_t initLed ( Led **led , uint8_t numero , uint8_t pin , uint32_t t_apagado,uint32_t t_prendido,uint8_t intensidad)
{
    //!apunto las funciones de leds
    (*led)->Intensidad = Intensidad_;
    (*led)->ledOn = ledOn_;
    (*led)->ledOff = ledOff_;
    (*led)->ledToggle = ledToggle_;
    (*led)->Parpadear = Parpadear_;
    (*led)->Set_Intensidad_Porcentaje = Set_Intensidad_Porcentaje_;
    (*led)->Set_Tiempo_Apagado = Set_Tiempo_Apagado_;
    (*led)->Set_Tiempo_Parpadear = Set_Tiempo_Parpadear_;
    (*led)->Set_Tiempo_Prendido = Set_Tiempo_Prendido_;
    
    //!Cargo los valores del led
    ( *led )->Nombre = numero;
    ( *led )->Puerto = PORT_1;//!Puerto 1 para los cuatro leds
    ( *led )->Pin = pin;//!Asigno numero de pin
    ( *led )->Tiempo_Apagado = t_apagado;//!Asigno tiempo preindido para parpadeo
	( *led )->Tiempo_Prendido = t_prendido;//!Asigno tiempo apagado para el parpadeo
    
    //!Inicializacion de contadores y flags del led
    ( *led )->Cont_Apagado = 0;
    ( *led )->Cont_Prendido = 0;
    ( *led )->F_off = 0;
    ( *led )->F_Tiempo = 0;
    ( *led )->Cont_Intensidad=100;
    ( *led )->F_on_off=0;
    
    //!cargo con el puntero del led en el array de flags para el timer
    F_leds[numero-1]=*led;
    
    //!Periodo intesidad o duty cycle es de 20ms ,los porcentajes de intensidad varian cada 5 porciento
    
    //!Cargo valores en los registros
    GPIO_SetDir((*led)->Puerto,(*led)->Pin,OUT); //!Setea el pin como salida
	GPIO_PinWrite((*led)->Puerto,(*led)->Pin,OFF); //! Empieza apagado
    
    //!Compruebo que los valores cargados sean correctos
    if(( *led )->Set_Intensidad_Porcentaje(led,intensidad) != OK)
        return 0;//! Retorna cero por error
    if(( *led )->Set_Tiempo_Parpadear(led) != OK)
        return 0;//! Retorna cero por error
    return 1;//!Retorna el puntero del led si no hubo error
}

/**
 * @brief Levanta Flags de Leds
 * @details se levanta los flags "F_Tiempo" de los leds cargados en el vector de punteros a Led "F_leds[]" 
 */

void FlagsLeds_On(void)
{
    uint8_t i;
    for(i=0;i<4;i++)//!Se fija en cada posicion del vector "F_leds"
    {
        if(F_leds[i] != 0)//!se fija si hay una direccion guardada
        {
            F_leds[i]->F_Tiempo=1;//!Levanta el flag F_Tiempo del led
        }
    }
}
