/**
  ******************************************************************************
  * @file    test_Leds.c
  * @author  Diego Quispe
  * @version 1.0
  * @date    04/07/2018
  * @brief   Funciones de testeo de LEDs
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#include "Leds.h"
#include <stdlib.h>
//#include <LPC17xx.h>

uint8_t Flag=0;

void test_leds (void)
{
    static Led *led1,*led2,*led3,*led4;
    static Led led01,led02,led03,led04;
    static uint8_t i=0;
    static uint32_t cont=0;
    static uint8_t initialized = 0;
    
    led1=&led01;
    led2=&led02;
    led3=&led03;
    led4=&led04;
    
    if(!initialized)
    {
        initialized=1;
        
        initLed(&led1,1,LED_1,1000,1000,100);//!Led_1
        initLed(&led2,2,LED_2,0,500,10);//!Led_2
        initLed(&led3,3,LED_3,2000,1000,100);//!Led_3
        initLed(&led4,4,LED_4,0,1500,10);//!Led_4
        
        //SysTick_Config(SystemCoreClock/1000);//interrumpe cada un milisegundo
    }
    //if(Flag == 1)
    //{
        led1->Parpadear(led1);
        led2->Parpadear(led2);
        led3->Parpadear(led3);
        led4->Parpadear(led4);
        if(led2->F_Tiempo == 1)
            cont++;
        if(cont==100)
        {
            led2->Set_Intensidad_Porcentaje(&led2,100-(i*5));
            i++;
            if(i==20)
            {
                i=0;
            }
            cont=0;
        }
        Flag = 0;
    //}
}
/*codigo para el testeo del systick sin CMSIS
void SysTick_Handler (void)
{
    static uint16_t i=0;
    i++;
    Flag=1;
    FlagsLeds_On();
}
*/
