/**
  ******************************************************************************
  * @file    led.h
  * @author  Sergio Alberino
  * @version 1.1
  * @date    18/07/2017
  * @brief   Funciones para el control de LEDs
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
    */

#ifndef _LED_H_
#define _LED_H_
#include <stdint.h>
//pines  0 1 4 8 , puerto 1 para todos
#define OK		 0
#define ERRORS	 1

/* Definicion de los pines asociados a cada LED */
typedef enum
{
    LED_1 = 0,
    LED_2 = 1,
    LED_3 = 4,
    LED_4 = 8,
}LEDs;


/*!
    \typedef Struct Led
    \brief cambio el nombre de "struct Led" a "Led"
*/
typedef struct Led Led;
/*!
    \struct Led
    \brief Es la estructura que tiene todas las variables y funciones para el uso de leds
*/
struct Led
{
    uint8_t Nombre;//!nombre del led
	uint8_t Pin;//!pin del led
	uint8_t Puerto;//!puerto del led
	uint8_t Intensidad_Porcentaje;//!intensidad con la que brilla el led
	uint32_t Tiempo_Prendido;//!tiempo que el led permanece prendido
	uint32_t Tiempo_Apagado;//!tiempo que el led permanece apagado
	uint32_t Tiempo_Parpadear;//!Periodo total de parpadeo ,osea suma de Tiempo_Prendido y Tiempo Apagado
	
    //!Contadores y flags del led
    uint32_t Cont_Prendido;
    uint32_t Cont_Apagado;
    uint8_t F_off;
    uint8_t F_Tiempo;
    uint8_t Cont_Intensidad;
    uint8_t F_on_off;

	//!Periodo intesidad o duty cycle es de 20ms ,los porcentajes de intensidad varian cada 5 porciento
    //!Periodo de intensidad fijo
    
	/**
	* @brief Inversion de estado de LED.
    * @details Todos los LEDs estan asociados al Puerto 1.
    * @param [in,out] led LED al que se le quiere invertir el estado.
	*/
	void ( *ledToggle ) ( Led * );
	
	/**
    * @brief Encendido de LED.
    * @details Todos los LEDs estan asociados al Puerto 1.
    * @param [in,out] led LED que se quiere encender.
	*/
	void ( *ledOn ) ( Led * );
	
	/**
	* @brief Apagado de LED.
    * @details Todos los LEDs estan asociados al Puerto 1.
    * @param [in,out] led LED que se quiere apagar.
	*/
	void ( * ledOff ) ( Led * );
	
	/**
	* @brief Regula intensidad leds.
    * @details Mediante el timer.
    * @param [in,out] led LED al cual se le quiere regular la intensidad.
	*/
	void ( *Intensidad ) ( Led * );
	
	/**
	* @brief Hacer que el led parpadee.
    * @details Mediante la utilizacion del timer hago que el led se prenda y apage durante cierto tiempo.
    * @param [in,out] led LED el cual se quiere hacer parpadear.
	*/
	void ( *Parpadear ) ( Led * ); 
	
	/**
	* @brief Setea el valor de intensidad.
    * @details Funcion que setea el valor de porcentaje con el que brilla el led.
    * @param [in,out] led LED el cual se quiere modificar.
    * @param [in]Intensidad_Porcentaje Porcentaje al que se le va a setear el LED.
    * @return Devuelve 0 si se pudo setear, 1 si hay error.
	*/
	uint8_t ( *Set_Intensidad_Porcentaje ) ( Led **, uint8_t );
	
	/**
	* @brief Setea el valor del tiempo prendido.
    * @details Funcion que setea el valor del tiempo que se mantiene prendido el led.
    * @param [in,out] led LED el cual se quiere modificar.
    * @param [in] Tiempo_Prendido Valor del tiempo que el LED va estar prendido.
    * @return Devuelve 0 si se pudo setear, 1 si hay error.
	*/
	uint8_t ( *Set_Tiempo_Prendido ) ( Led **, uint32_t );
	
	/**
	* @brief Setea el valor del tiempo apagado.
    * @details Funcion que setea el valor del tiempo que se mantiene apagado el led.
    * @param [in,out] led LED el cual se quiere modificar.
    * @param [in] Tiempo_Apagado Valor del tiempo que el LED va a estar apagado. 
    * @return Devuelve 0 si se pudo setear, 1 si hay error.
	*/
	uint8_t ( *Set_Tiempo_Apagado ) ( Led **, uint32_t );
	
	/**
	* @brief Setea el valor del tiempo en que cambia de estado.
    * @details Funcion que setea el valor del tiempo que cambia el flag de la funcion de parpadeo lo que hace que pase del estado prendido a apagado, o al revez.
    * @param [in,out] led LED el cual se quiere modificar.
    * @return Devuelve 0 si se pudo setear, 1 si hay error.
	*/
	uint8_t ( *Set_Tiempo_Parpadear ) ( Led ** );
			
};
	/**
	* @brief Inicializacion de LEDs.
    * @details Definicion y configuracion de cada LED.
    * @param [in] led ,puntero a led a modificar.
    * @param [in] pin ,pin de LED que va a ser inicializado.
    * @param [in] numero ,numero de led a ser cargado
    * @param [in] t_apagado ,tiempo apagado del parpadeo a ser cargado
    * @param [in] t_prendido ,tiempo prendido del parpadeo a ser cargado
    * @param [in] intensidad ,porcentaje de intensidad del led
    * @return Devuelve 1 por exito,devuelve 0 si hubo error
	*/
	uint8_t initLed (  Led** , uint8_t , uint8_t , uint32_t , uint32_t , uint8_t );

/**
 * @brief Levanta Flags de Leds
 * @details se levanta los flags "F_Tiempo" de los leds cargados en el vector de punteros a Led "F_leds[]" 
 */

void FlagsLeds_On(void);

extern Led* F_leds[4];

#endif	// End _LED_H_


