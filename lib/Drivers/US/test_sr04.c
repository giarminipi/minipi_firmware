/**
  ******************************************************************************
  * @file    	test_us.c
  * @author		Cesar Mejia
  * @version	1.0
  * @date			20/05/2018
  * @brief		Programa de testeo del ultra sonido
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#include <Leds.h>
#include <sr04.h>
#include <cmsis_os2.h>

#define PORT_1                  1

#define PORT_TRIGGER_US1        PORT_1
#define PORT_TRIGGER_US2        PORT_1
#define PORT_TRIGGER_US3        PORT_1
#define PORT_TRIGGER_US4        PORT_1

#define PORT_ECHO_US1           PORT_1
#define PORT_ECHO_US2           PORT_1
#define PORT_ECHO_US3           PORT_1
#define PORT_ECHO_US4           PORT_1

// Pines de conexion
#define PIN_TRIGGER_US1         14
#define PIN_TRIGGER_US2         15
#define PIN_TRIGGER_US3         16
#define PIN_TRIGGER_US4         17

#define PIN_ECHO_US1            18
#define PIN_ECHO_US2            19
#define PIN_ECHO_US3            26
#define PIN_ECHO_US4            27

#define TRIGGER_DURATION_MSEG   1
#define MAX_RANGE_CM            10
#define MIN_RANGE_CM            2


bool isInRange(const float distance, const float rangeMin, const float rangeMax) {
    return ( ( distance >= rangeMin) && ( distance <= rangeMax) ) ? true : false;
}

void us1callback (float distance){
    isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM) ? ledOn(LED_1) : ledOff(LED_1);
}
void us2callback (float distance){
    isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM) ? ledOn(LED_2) : ledOff(LED_2);
}
void us3callback (float distance){
    isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM) ? ledOn(LED_4) : ledOff(LED_4);
}
void us4callback (float distance){
    isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM) ? ledOn(LED_3) : ledOff(LED_3);
}

SR04Data us1_data = {
    .callback = us1callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = PORT_TRIGGER_US1,
    .trigger_pin = PIN_TRIGGER_US1,

    .echo_port = PORT_ECHO_US1,
    .echo_pin = PIN_ECHO_US1,
};

SR04Data us2_data = {
    .callback = us2callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = PORT_TRIGGER_US2,
    .trigger_pin = PIN_TRIGGER_US2,

    .echo_port = PORT_ECHO_US2,
    .echo_pin = PIN_ECHO_US2,
};

SR04Data us3_data = {
    .callback = us3callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = PORT_TRIGGER_US3,
    .trigger_pin = PIN_TRIGGER_US3,

    .echo_port = PORT_ECHO_US3,
    .echo_pin = PIN_ECHO_US3,
};

SR04Data us4_data = {
    .callback = us4callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = PORT_TRIGGER_US4,
    .trigger_pin = PIN_TRIGGER_US4,

    .echo_port = PORT_ECHO_US4,
    .echo_pin = PIN_ECHO_US4,
};

void test_sr04(void)
{
	initLed();
    
    osThreadAttr_t us_att = {
      .stack_size = 1024,
      .priority = osPriorityISR,
    };

    us_att.name = "US1";
    osThreadNew(SR04Task, &us1_data, &us_att);
    
    us_att.name = "US2";
    osThreadNew(SR04Task, &us2_data, &us_att);
    
    us_att.name = "US3";
    osThreadNew(SR04Task, &us3_data, &us_att);
    
    us_att.name = "US4";
    osThreadNew(SR04Task, &us4_data, &us_att);
}
