/**
  ******************************************************************************
  * @file    Us.c
  * @author  Juan Manuel Gabis & Joaquín Toranzo Calderón
  * @version 4.0
  * @date    18/08/2017
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#include <sr04.h>
#include <GPIO_LPC17xx.h>   
#include <PIN_LPC17xx.h>
#include <cmsis_os2.h>
#include <stdbool.h>

#define US_CAPTURE_EVENT_FLAG 1

#define US_ON           1
#define US_OFF          0

#define NUM_TIMERS      4
#define NUM_CAPTURES    2

#define CYCLE           50  //mseg
#define TIME_OUT        40  //mseg

typedef struct{
    uint32_t tickUp;
    uint32_t tickDown;
}Ticks;

static Ticks* threads_ticks[NUM_TIMERS*NUM_CAPTURES] = {0};
static osThreadId_t thread_ids[NUM_TIMERS*NUM_CAPTURES] = {0};

osMutexId_t sr04_mtx = NULL;

static uint32_t SR04_checkTicks(uint32_t tickCountUp, uint32_t tickCountDown) {
  if( tickCountUp > tickCountDown)
    return tickCountDown + (INT32_MAX - tickCountUp);
  else
    return tickCountDown - tickCountUp;
}

static float SR04_getMeasure(uint32_t pulse_duration, SR04_UnitType unit) {
  switch(unit) {
    case METRIC_M:
        return (float) pulse_duration * (100/58.0);
    case METRIC_MM:
        return (float) pulse_duration * (10/58.0);
    case METRIC_CM:
        return (float) pulse_duration * (1/58.0);
    case IMPERIAL_INCH:
        return (float) pulse_duration * (1/148.0);
    case IMPERIAL_MILS:
        return (float) pulse_duration * (1000/148.0);
    default:
        return pulse_duration;
  }
}

static void get_timer_and_capture (const uint8_t echo_port, const uint8_t echo_pin, uint8_t* capture_channel, LPC_TIM_TypeDef** timer, uint8_t* timer_num) {
    // TODO Hacer algo si estan mal los pines y/o puertos
    
    if(echo_port == 1){
        if(echo_pin == 26){
            // Timer 0 Capture 0
            *timer = LPC_TIM0;
            *timer_num = 0;
            *capture_channel = 0;
        }
        if(echo_pin == 27){
            // Timer 0 Capture 1
            *timer = LPC_TIM0;
            *timer_num = 0;
            *capture_channel = 1;
        }
        if(echo_pin == 18){
            // Timer 1 Capture 0
            *timer = LPC_TIM1;
            *timer_num = 1;
            *capture_channel = 0;
        }
        if(echo_pin == 19){
            // Timer 1 Capture 1
            *timer = LPC_TIM1;
            *timer_num = 1;
            *capture_channel = 1;
        }
    }
    if(echo_port == 0){
        if(echo_pin == 4){
            // Timer 2 Capture 0
            *timer = LPC_TIM2;
            *timer_num = 2;
            *capture_channel = 0;
        }
        if(echo_pin == 5){
            // Timer 2 Capture 1
            *timer = LPC_TIM2;
            *timer_num = 2;
            *capture_channel = 1;
        }
        if(echo_pin == 23){
            // Timer 3 Capture 0
            *timer = LPC_TIM3;
            *timer_num = 3;
            *capture_channel = 0;
        }
        if(echo_pin == 24){
            // Timer 3 Capture 1
            *timer = LPC_TIM3;
            *timer_num = 3;
            *capture_channel = 1;
        }
    }
}

static void get_port_pin (const uint8_t capture_channel, const uint8_t timer_num, uint8_t *echo_port, uint8_t *echo_pin) {
    // TODO Hacer algo si estan mal los pines y/o puertos
    
    switch(timer_num ){
        case 0:
            *echo_port = 1;
            *echo_pin = (capture_channel == 0) ? 26 : 27;
            break;
        case 1:
            *echo_port = 1;
            *echo_pin = (capture_channel == 0) ? 18 : 19;
            break;
        case 2:
            *echo_port = 0;
            *echo_pin = (capture_channel == 0) ? 4 : 5;
            break;
        case 3:
            *echo_port = 0;
            *echo_pin = (capture_channel == 0) ? 23 : 24;
            break;
    }
    return;
}

void SR04Task (void* params) {
    SR04Data* data = (SR04Data*) params;    
    uint32_t tick, pulse = 0;
    Ticks thread_ticks = {.tickUp = 0, .tickDown = 0};
    const osThreadId_t thread_id = osThreadGetId();
    const char* thread_name = osThreadGetName(thread_id);
    float distance = 0;
    //osMutexId_t* sr04_mtx = NULL;
    
    if(!sr04_mtx)
        sr04_mtx = osMutexNew(NULL);
    
    // Obtengo el mutex
    osMutexAcquire(sr04_mtx, osWaitForever);
    
    // Get timer and capture channel
    LPC_TIM_TypeDef* timer;
    uint8_t capture_channel, timer_num;
    get_timer_and_capture (data->echo_port, data->echo_pin, &capture_channel, &timer, &timer_num);

    // Configure Trigger
//    PIN_Configure(data->trigger_port, data->trigger_pin, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);
    GPIO_SetDir(data->trigger_port, data->trigger_pin, GPIO_DIR_OUTPUT);
    GPIO_PinWrite(data->trigger_port, data->trigger_pin, US_OFF);

    // Configurar los pines con resistencia de pulldown
    PIN_Configure(data->echo_port, data->echo_pin, PIN_FUNC_3, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);

    if(timer->TCR != TIM_ENABLE) {
        // Initialize timer 1, prescale count time of 1uS
        TIM_TIMERCFG_Type TMR_Config = {
            .PrescaleOption = TIM_PRESCALE_USVAL,       /** Seteo del prescaler del Timer en useg */
            .PrescaleValue = 1,                        /** Seteo del valor del prescaler del Timer en 1useg */       
        };
        TIM_Init(timer, TIM_TIMER_MODE, (void *)&TMR_Config);

        // Reseteo del Timer
        TIM_ResetCounter(timer);
        
        // Encender Timer
        TIM_Cmd(timer, ENABLE);
        
        // Habilito interrupciones del timer
        NVIC_EnableIRQ((IRQn_Type) (TIMER0_IRQn + timer_num));
    }

    // Configure Capture
    TIM_CAPTURECFG_Type TIM_CaptureConfig = {
        .CaptureChannel = capture_channel,      /** Capture channel */
        .RisingEdge = ENABLE,                   /** Enable capture on rising edge */
        .FallingEdge = ENABLE,                  /** Enable capture on falling edge */
        .IntOnCaption = ENABLE,                 /** Generate capture interrupt */
    };
    TIM_ConfigCapture(timer, &TIM_CaptureConfig);
    
    // Set Thread data
    thread_ids[ timer_num * NUM_CAPTURES + capture_channel] = thread_id;
    threads_ticks[ timer_num * NUM_CAPTURES + capture_channel] = &thread_ticks;

     // Libero el mutex para que pueda continuar otro US
    osMutexRelease(sr04_mtx);
    
    for(;;) {
        osMutexAcquire(sr04_mtx, osWaitForever);
        osThreadFlagsClear(US_CAPTURE_EVENT_FLAG);

        thread_ticks.tickDown = 0;
        thread_ticks.tickUp = 0;
        
        // Trigger US for measurement
        GPIO_PinWrite(data->trigger_port, data->trigger_pin, US_ON);
        osDelay(data->trigger_duration_mseg * (osKernelGetTickFreq()/1000) );
        GPIO_PinWrite(data->trigger_port, data->trigger_pin, US_OFF);
               
        tick = osKernelGetTickCount();
        
        // Wait for flags
        if( osThreadFlagsWait(US_CAPTURE_EVENT_FLAG, osFlagsWaitAll, TIME_OUT * (osKernelGetTickFreq()/1000)) == US_CAPTURE_EVENT_FLAG ) {
            pulse = SR04_checkTicks(thread_ticks.tickUp,  thread_ticks.tickDown);
            distance = SR04_getMeasure(pulse, data->unit);
            if(pulse >= 150 && pulse <= 25000)
                data->callback(distance);
        }
        
        // Libero el mutex para que pueda continuar otro US
        osMutexRelease(sr04_mtx);
        
        // Espero lo que reste de tiempo hasta llegar a CYCLE mseg para volver a disparar el US
        osDelayUntil( CYCLE * (osKernelGetTickFreq()/1000) + tick );
    }
}


void setFlag(LPC_TIM_TypeDef* timer, const uint8_t timer_num){
    uint8_t capture = 0xFF, echo_port, echo_pin, idx;
    
    // Clear pending interrupts
    if( TIM_GetIntStatus(timer, TIM_CR0_INT) == SET ) {capture = 0;}
    if( TIM_GetIntStatus(timer, TIM_CR1_INT) == SET ) {capture = 1;}
    
    if(capture != 0xFF){
        // Get port and pin
        get_port_pin (capture, timer_num, &echo_port, &echo_pin);
        idx = timer_num * NUM_CAPTURES + capture;
        
        // Save ticks
        if( GPIO_PinRead(echo_port, echo_pin) )
             threads_ticks[idx]->tickUp = TIM_GetCaptureValue (timer, (TIM_COUNTER_INPUT_OPT) capture);
        else
             threads_ticks[idx]->tickDown = TIM_GetCaptureValue (timer, (TIM_COUNTER_INPUT_OPT) capture);
        
        // If both ticks set, send message
        if ( threads_ticks[idx]->tickUp && threads_ticks[idx]->tickDown )
            osThreadFlagsSet(thread_ids[idx], US_CAPTURE_EVENT_FLAG);
        
        TIM_ClearIntPending(timer, (TIM_INT_TYPE) (TIM_CR0_INT + capture));
    }
}

void TIMER0_IRQHandler(void) {
    setFlag(LPC_TIM0, 0);
}

void TIMER1_IRQHandler(void) {
    setFlag(LPC_TIM1, 1);
}

void TIMER2_IRQHandler(void) {
    setFlag(LPC_TIM2, 2);
}

void TIMER3_IRQHandler(void) {
    setFlag(LPC_TIM3, 3);
}

