/**
  ******************************************************************************
  * @file    Us.c
  * @author  Juan Manuel Gabis & Joaquín Toranzo Calderón
  * @version 4.0
  * @date    18/08/2017
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef __SR04_H__
#define __SR04_H__

#include <stdint.h>
#include <LPC17xx.h>
#include <lpc17xx_timer.h>
#include <cmsis_os2.h>

/**
  * @typedef SR04_UnitType
  * @brief   Unidades de conversion de distancia.
  */
typedef enum {
  METRIC_M = 0,       /* Metros */
  METRIC_CM,          /* Centímetros */
  METRIC_MM,          /* Milímetros */
  IMPERIAL_INCH,      /* Pulgadas */
  IMPERIAL_MILS,      /* Milésimas de pulgada */
} SR04_UnitType;

typedef struct _SR04Data
{
    uint32_t trigger_duration_mseg;  
    uint8_t trigger_port;
    uint8_t trigger_pin;

    uint8_t echo_port;
    uint8_t echo_pin;

    void (*callback) (float distance);
    SR04_UnitType unit;
} SR04Data;

void SR04Task (void* params);

#endif  // End __SR04_H__
