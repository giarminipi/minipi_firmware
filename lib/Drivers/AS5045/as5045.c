/**
  ******************************************************************************
  * @file       as5045.c
  * @author     Alejandro Gastón Alvarez
  * @version    v1.0
  * @date       09/09/2018
  * @brief      AS5045 driver for CMSIS and using RTE_Device
  ******************************************************************************
  * attention
  * The sensor works in Daisy Chain Mode
  ******************************************************************************
	*/

#include <as5045.h>
#include <cmsis_os2.h>
#include <string.h>
#include <assert.h>


static ARM_DRIVER_SPI* SPIdrv;
static AS5045_Data_Type sensor[AS5045_NUM_SENSORS];

// Seteo el valor a enviar (uso uint8_t porque es el que utiliza el driver por estar seteado en 8bits)
#define CEILING(x,y) (((x) + (y) - 1) / (y))
#define AS5045_DATA_SIZE CEILING(1+19*AS5045_NUM_SENSORS, 8)
static uint8_t data[AS5045_DATA_SIZE];

callback _user_callback;

/* Functions -----------------------------------------------------------------------------------------*/
void AS5045_Init( ARM_DRIVER_SPI *Driver_SPI, RingBuffer* clt_buffer, const size_t buff_size, callback  user_callback )
{
	// Check that at least one of them are is set but not both
	assert( (clt_buffer != NULL || user_callback != NULL) && ( ((bool) clt_buffer & (bool)user_callback) == NULL) );
	
	// If encoder_buffer is NULL or buff_size is NULL ===> No RingBuffer
	if( encoder_buffer != NULL && buff_size != NULL )
	{
		// Create Ring Buffer
		encoder_buffer = clt_buffer;
		ringBuf_init ( encoder_buffer, buff_size * sizeof(AS5045_Data_Type) , false );
	}
	else
		_user_callback = user_callback;

	// Seteo los pines P0.17-18-19 (SCK,MOSI,MISO) como SSI-SSP
	SPIdrv = Driver_SPI;
	
	/* Initialize the SPI driver */
	SPIdrv->Initialize(SPI_callback);

	/* Power up the SPI peripheral */
	SPIdrv->PowerControl(ARM_POWER_FULL);
	
	// Configure the SPI to Master, 8-bit mode @50 kBits/sec
	//El SSEL lo hago manual porque sino me corta la comunicación
	SPIdrv->Control(ARM_SPI_MODE_MASTER | ARM_SPI_CPOL1_CPHA0 | ARM_SPI_MSB_LSB | ARM_SPI_SS_MASTER_SW | ARM_SPI_DATA_BITS(8), 50000);
	
	// Desactivo la lectura de los senosres	(SS line = INACTIVE)
	SPIdrv->Control(ARM_SPI_CONTROL_SS, ARM_SPI_SS_INACTIVE);
}

void AS5045_ask_data ( void )
{	
	// Activo la lectura de los senosres	(SS line = ACTIVE)
	SPIdrv->Control(ARM_SPI_CONTROL_SS, ARM_SPI_SS_ACTIVE);
	
	// Envío 40 bits (5 de 8 bits)
//	SPIdrv->Send(&data, sizeof(data));
	
	// Receive 8 bytes of reply
	SPIdrv->Receive(data, AS5045_DATA_SIZE);
}

AS5045_Status AS5045_validate_data (AS5045_Data_Type *data)
{
	// Como AS5045_Data_Type es de 32bits puedo utilizar esta variable y después la casteo
	AS5045_Status status = AS5045_OK;
	
	// Check Parity
	uint32_t* sensor = (uint32_t*) data;
	uint8_t parity = 0;
	// Comienzo desde 1 para no tener en cuenta el bit de Even Parity
	for (uint32_t i=1; i < sizeof(sensor)*8; i++) 
		parity ^= ( (*sensor >> i) & 0x01 );
	if (data->EvenPar != parity)
		status += AS5045_PARITY_ERROR;

	// Check Offset Compensation Finished (OCF)
	if (!data->offset_comp_finished)
		status += AS5045_COMP_ALGO_NOT_FINISHED;

	// Check Cordic Overflow
	if (data->cordic_overflow)
		status += AS5045_CORDIC_OVERFLOW;
	
	// Linearity Alarm check
	if (data->linearity_alarm)
		status += AS5045_MAGNET_BAD_ALIGMENT;

	// Check Magnet Status
	if (data->mag_inc && data->mag_dec)
        status += AS5045_MAGNET_UNSTABLE;
	else if (data->mag_inc || data->mag_dec)
		status += AS5045_MAGNET_MOVING;
	
	return status;
}

AS5045_Data_Type* AS5045_read_data (void)
{
	uint32_t data_int;
		
	// Arrange data (data is package in 10 bits)
	data_int = ((data[0] & 0x7F) << 21) + ((data[1] & 0xF8) << 13);	// dejo en los 16 bits más altos el valor de la posición angular
	data_int += ((data[1] & 0x06) << 3) + ((data[2] & 0xE0) >> 5); // dejo en los 16 bits más bajos los datos de información del encoder
	
	memcpy( &sensor[0], &data_int, sizeof(uint32_t) );
	
	data_int = ((data[2] & 0x0F) << 24) + ((data[3] & 0xFF) << 16);	// dejo en los 16 bits más altos el valor de la posición angular
	data_int += ((data[4] & 0xFC) >> 2); // dejo en los 16 bits más bajos los datos de información del encoder
	
	memcpy( &sensor[1], &data_int, sizeof(uint32_t) );
		
	return sensor;
}

void SPI_callback(uint32_t event)
{
    switch (event)
    {
    case ARM_SPI_EVENT_TRANSFER_COMPLETE:
		
		// Desactivo la lectura de los senosres	(SSEL line = INACTIVE)
		SPIdrv->Control(ARM_SPI_CONTROL_SS, ARM_SPI_SS_INACTIVE);

		// Call whoever corresponds
		if (_user_callback)
			_user_callback();
		else
            AS5045_read_data();
		
        break;
    case ARM_SPI_EVENT_DATA_LOST:
        break;
    case ARM_SPI_EVENT_MODE_FAULT:
        break;
    }
}

