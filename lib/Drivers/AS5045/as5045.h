/**
  ******************************************************************************
  * @file     as5045.h
  * @author   Alejandro Gastón Alvarez
  * @version  v1.0
  * @date     09/09/2016
  * @brief    AS5045 driver for CMSIS and using RTE_Device
  ******************************************************************************
  * @attention
  * The sensor works in Daisy Chain Mode
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _AS5045_H
#define _AS5045_H


/* Configuration ------------------------------------------------------------------*/
#define AS5045_NUM_SENSORS 2


/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stddef.h>
#include <ring_buffer.h>
#include <Driver_SPI.h>


/* Typedef -----------------------------------------------------------------------------------------*/
static RingBuffer* encoder_buffer;

/** @typedef */
typedef void ( *callback ) ( void );

/** @enum  AS5045_Mode 
 *  @brief Sensor mode of operation
 */
typedef enum
{
    AS5045_Pulling,
    AS5045_Interrupt,
} AS5045_Mode;

/** @struct AS5045_Data_Type 
 *  @brief  Sensor data type
 */
typedef struct
{
    uint8_t EvenPar             : 1;    /**< Sensor Even Parity bit */
    bool mag_dec                : 1;    /**< Sensor Magnetic filed decreasing bit*/
    bool mag_inc                : 1;    /**< Sensor Magnetic filed increasing bit */
    bool linearity_alarm        : 1;    /**< Sensor Linearity Alarm bit */
    bool cordic_overflow        : 1;    /**< Sensor Cordic Overflow bit */
    bool offset_comp_finished   : 1;    /**< Sensor Offset Compensation Algorithm bit */
    uint16_t                    : 0;    
    uint16_t ang_pos            : 12;   /**< Sensor Angular Position word(12bits) */
} AS5045_Data_Type;

/** @enum AS5045_Status 
 *  @brief  Sensor status indicator
 */
typedef enum
{
    AS5045_OK = 0,                          /**< Sensor status OK */
    AS5045_PARITY_ERROR = 0x01,             /**< Sensor Parity Error */
    AS5045_COMP_ALGO_NOT_FINISHED = 0x02,   /**< Sensor Offset Compensation Algorithm isn't finished */
    AS5045_CORDIC_OVERFLOW = 0x04,          /**< Sensor Cordic Overflow error and implies that the data is invalid (absolute output maintains the last valid angular value). */
    AS5045_MAGNET_BAD_ALIGMENT = 0x08,      /**< Sensor Magnet field generates a critical output linearity. Data may still be used but can contain invalid data. */
    AS5045_MAGNET_MOVING = 0x10,            /**< Sensor Magenet moving away or closer to the chip. This state is dynamic and only active while the magnet is moving to the same direction. */
    AS5045_MAGNET_UNSTABLE = 0x20,          /**< Sensor Magnetic field is ~<45mT or >~75mT. It is still possible to operate the AS5045 in this range, but not recommended. */
}AS5045_Status;

    
/**
 * @brief     Initialized AS5045 sensors
 * @details   Analyze the received frame (position and status indicators) and validats the data
 * @param[in] Driver_SPI SPI driver structure where the sensor is connected (typically set in RTE_Device.h and defined in uControler HAL)
 * @param[in] encoder_buffer Ringbuffer for the sensor to use. User can set to NULL if it's not wanted, but a user_callback function must be set
 * @param[in] buff_size Ringbuffer size buffer (number of AS5045_Data_Type data)
 * @param[in] user_callback User callback function
 */
void AS5045_Init ( ARM_DRIVER_SPI* Driver_SPI, RingBuffer* encoder_buffer, const size_t buff_size, callback  user_callback );

/**
 * @brief    Ask's data to sensor AS5045 sensors
 * @details  Ask's data to sensor AS5045 sensors (It uses the interruption vector, so this function is non-blocking)
 */
void AS5045_ask_data ( void );

/**
 * @brief    Validate frame
 * @details  Analyze the received frame (position and status indicators) and validats the data
 * @param    data received data
 * @return   AS5045_Status
 */
AS5045_Status AS5045_validate_data (AS5045_Data_Type *data);

/**
 * @brief    SPI callback function
 * @details  
 * @param    event  
 */
void SPI_callback(uint32_t event);

/**
 * @brief    Reads data frame
 * @details  Reads data frame and outputs it as a AS5045_Data_Type
 * @return   AS5045_Data_Type
 */
AS5045_Data_Type* AS5045_read_data (void);
    
#endif // _AS5045_H_
