/**
  ******************************************************************************
  * @file       test_as5045.c
  * @author     Alejandro Gastón Alvarez
  * @version    1.0
  * @date       21/09/2018
  * @brief      Programa de testeo del encoder AS5045
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
    */
    
#ifdef TEST_AS5045

#include <Driver_SPI.h>
#include <as5045.h>
#include <stdbool.h>

#define FLAG 0x1

extern ARM_DRIVER_SPI Driver_SPI0;
static osEventFlagsId_t read_data_event;
const osThreadAttr_t test_as5045_task_attr = {
  .stack_size = 1024    // Create the thread stack with a size of 1024 bytes
};


void as5045_callback ()
{
    osEventFlagsSet(read_data_event, FLAG);
}

void test_as5045_task( void *argument ){
    
    /* Initialization */
    AS5045_Init( &Driver_SPI0, NULL, NULL, as5045_callback );
    AS5045_Status status;
    AS5045_Data_Type* sensors_data;
    static int count = 0;
    read_data_event = osEventFlagsNew(NULL);
    
    /* Start Infinite Loop */
    for(;;){
        // Ask for data
        AS5045_ask_data();
        
        // Wait for an event genreation (BLOCKING FUNCTION)
        if (osEventFlagsWait (read_data_event, FLAG, osFlagsWaitAll, osWaitForever) == FLAG){
    
            // Read data
            sensors_data = AS5045_read_data();
    
            // Validate each sensor data
            for (uint8_t sensor_idx = 0; sensor_idx < 2; sensor_idx++)
            {
                status = AS5045_validate_data (&sensors_data[sensor_idx]);
        
                if (status == AS5045_OK || status == AS5045_MAGNET_UNSTABLE)
                    count++;
            }
        }
        else
        {
            /* "Error Handler" */
        }
    }
}

#endif
