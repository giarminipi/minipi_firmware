/**
 ******************************************************************************
 * @file			test_Pulsadores.c
 * @author	    	Alejandro Gastón Alvarez
 * @version	    	0.1
 * @date			05/11/2018
 * @brief			Programa de testeo de Pulsadores
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#include <Pulsadores.h>
#include <cmsis_os2.h>
#include <Leds.h>

void pbEvent (void){
    ledToggle(LED_1);
}

const Pulsador_Params pp = {
    .port = 1,
    .pin = 9,
    .onPress = pbEvent,
    .onRelease = pbEvent,
    .sampling_time = 1,
    .min_samples = 20
};

void test_pulsador(void){
    
    initLed();
    
    const osThreadAttr_t attributes = { 
        .stack_size = 128
    };
    
    osThreadNew(pulsadorTsk, (void*) &pp, &attributes);
}
