/**
  ******************************************************************************
  * @file    Pulsadores.h
  * @author  
  * @version 1.1
  * @date    16/06/2018
  * @brief   Funciones para el control de Pulsadores
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _PULSADORES_H_
#define _PULSADORES_H_

#include <stdlib.h>
#include <stdint.h>

typedef struct {
    uint8_t port;
    uint8_t pin;
    uint32_t sampling_time;
    uint8_t min_samples;
    void (*onPress) ( void );
    void (*onRelease) ( void );
}Pulsador_Params;

typedef enum {
    BUTTON_DEBOUNCING = -1,
    BUTTON_PRESSED = 0,
    BUTTON_RELEASED = 1,
}Button_State;

void pulsadorTsk (void *parameters);

#endif    // End _PULSADORES_H_

