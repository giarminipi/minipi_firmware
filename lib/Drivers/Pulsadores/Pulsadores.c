/**
  ******************************************************************************
  * @file    Pulsadores.c
  * @author  Alejandro Gastón Alvarez
  * @version 0.1
  * @date    05/11/2018
  * @brief   Funciones para el control de Pulsadores
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#include <Pulsadores.h>
#include <cmsis_os2.h>
#include <GPIO_LPC17XX.h>

static void update_integrator(uint8_t* integrator, const uint8_t current_read, const uint8_t min_samples ){
    if( current_read ) {
        if( *integrator < min_samples )
            (*integrator)++;
    } else {
        if( *integrator > 0 )
            (*integrator)--;
    }
}

static Button_State debounce_logic (uint8_t* integrator,  const Button_State state, const uint8_t current_read, const uint8_t min_samples, uint8_t *counter) {
    // Update integrator with current read
    update_integrator(integrator, current_read, min_samples);

    // Do maximum times while
    if ( *counter < min_samples - 1){
        (*counter)++;
    }
    else {
        // Check if I'm outside the window
        if ( ! (*integrator < min_samples && *integrator > 0 ) )
            return *integrator ? BUTTON_RELEASED : BUTTON_PRESSED;
    }
    return BUTTON_DEBOUNCING;
}

void pulsadorTsk (void *params){
    
    Pulsador_Params* pp = (Pulsador_Params*) params;
    
    uint8_t integrator = pp->min_samples;
    uint8_t counter = 0;
    Button_State state = BUTTON_RELEASED;

    GPIO_SetDir(pp->port, pp->pin, GPIO_DIR_INPUT);
    
    for(;;){
        // Read pin
        Button_State current_read = (Button_State) GPIO_PinRead (pp->port, pp->pin);
        
        // If in debouncing ==> counter != 0 or if current state is different from state
        if( counter || current_read != state) {
            
            // Run debounce logic
            Button_State new_state = debounce_logic(&integrator, state, current_read, pp->min_samples, &counter);
            
            switch(new_state){
                case BUTTON_PRESSED:
                    integrator = 0;
                    counter = 0;
                    if (state != new_state && pp->onPress != NULL)
                        pp->onPress();
                        state = new_state;
                        break;
                    case BUTTON_RELEASED:
                    integrator = pp->sampling_time;
                    counter = 0;
                    if (state != new_state && pp->onRelease != NULL)
                        pp->onRelease();
                    state = new_state;
                        break;
                case BUTTON_DEBOUNCING:
                    default:    //ERROR
                        break;
                }
            }
        
        // Sleep the requiered time
        osDelay(pp->sampling_time * (osKernelGetTickFreq()/1000) );
    }
}
