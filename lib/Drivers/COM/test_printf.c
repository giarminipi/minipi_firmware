/**
 ******************************************************************************
 * @file			test_printf.c
 * @author		Alejandro Gast�n Alvarez
 * @version		1.0
 * @date			30/04/2016
 * @brief			Programa de testeo de printf por UART
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#include <string.h>

void test_printf() {
	char buff[100];
	
	// Recibe
	int i = 0;
	do
		buff[i++] = (char) stdin_getchar();
	while (buff[i-1] != 13);
	strcpy(&buff[i], "\n");

	// Return same buff
	printf("robot: %s", buff);
}
