/**
  ******************************************************************************
  * @file    	stdio.h
  * @author   Alejandro G Alvarez
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	

#include <Driver_USART.h>
#include <stdio.h>
 
// struct __FILE { int handle; /* Add whatever needed */ };
// FILE __stdout;
// FILE __stdin;


int stdio_init (void);
int stdout_putchar (int ch);
int stdin_getchar (void);
