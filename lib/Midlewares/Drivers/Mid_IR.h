/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _IR_H_
#define _IR_H_

#include "ir_line.h"

void ir1_rise (void);
void ir2_rise (void);
void ir3_rise (void);
void ir4_rise (void);

void ir1_fall (void);
void ir2_fall (void);
void ir3_fall (void);
void ir4_fall (void);

/**********************
    IR Data
***********************/
#if DRIVER_IR
const IR_Params ir_1 = {
    .port = 0,
    .pin = 24,
    .onRise = ir1_rise,
    .onFalling = ir1_fall,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_2 = {
    .port = 0,
    .pin = 23,
    .onRise = ir2_rise,
    .onFalling = ir2_fall,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_3 = {
    .port = 0,
    .pin = 25,
    .onRise = ir3_rise,
    .onFalling = ir3_fall,
    .sampling_time = 1,
    .min_samples = 20
};

const IR_Params ir_4 = {
    .port = 0,
    .pin = 26,
    .onRise = ir4_rise,
    .onFalling = ir4_fall,
    .sampling_time = 1,
    .min_samples = 20
};
#endif

#endif    // End _IR_H_
