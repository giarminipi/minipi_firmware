/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/
#include <cmsis_os2.h>
#include "Mid_AS5045.h"

/**********************
   5045 Functionality
***********************/
//no se usa en la maquina de estados, donde meto el test?

extern ARM_DRIVER_SPI Driver_SPI0;
extern osEventFlagsId_t read_data_event;



void as5045_callback (){
    osEventFlagsSet(read_data_event, FLAG);
}

void test_as5045_task( void *argument ){
    
    /* Initialization */
    AS5045_Init( &Driver_SPI0, NULL, NULL, as5045_callback );
    AS5045_Status status;
    AS5045_Data_Type* sensors_data;
    static int count = 0;
    read_data_event = osEventFlagsNew(NULL);
    
    /* Start Infinite Loop */
    for(;;){
        // Ask for data
        AS5045_ask_data();
        
        // Wait for an event genreation (BLOCKING FUNCTION)
        if (osEventFlagsWait (read_data_event, FLAG, osFlagsWaitAll, osWaitForever) == FLAG){
    
            // Read data
            sensors_data = AS5045_read_data();
    
            // Validate each sensor data
            for (uint8_t sensor_idx = 0; sensor_idx < 2; sensor_idx++)
            {
                status = AS5045_validate_data (&sensors_data[sensor_idx]);
        
                if (status == AS5045_OK || status == AS5045_MAGNET_UNSTABLE)
                    count++;
            }
        }
        else
        {
            /* "Error Handler" */
        }
    }
}

