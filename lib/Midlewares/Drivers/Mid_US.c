/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/
#include "Mid_US.h"//borrar lineas de abajo
#include <stdbool.h>
//#include <Leds.h> 
#include "GPIO_LPC17xx.h"

extern uint8_t global_input2;
/**********************
    US Functionality
***********************/
bool isInRange(const float distance, const float rangeMin, const float rangeMax) {
    return ( ( distance >= rangeMin) && ( distance <= rangeMax) ) ? true : false;
}

void us1callback (float distance){
    if (isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM)){
        global_input2 |= 0x10;
    }
    else{
        global_input2 &= 0xEF;
    }
}
void us2callback (float distance){
    if (isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM)){
        global_input2 |= 0x20;
    }
    else {
        global_input2 &= 0xDF;
    }
}
void us3callback (float distance){
    if (isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM)){
        global_input2 |= 0x40;
    }
    else {
        global_input2 &= 0xBF;
    }
}
void us4callback (float distance){
    if (isInRange(distance, MIN_RANGE_CM, MAX_RANGE_CM)){
        global_input2 |= 0x80;
    }
    else{
        global_input2 &= 0x7F;
    }
}
