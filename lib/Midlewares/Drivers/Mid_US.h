/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _MID_US_H_
#define _MID_US_H_
#include <sr04.h>
#include <stdbool.h>


#define FLAG_SWITCH_RELEASE   	1
#define TRIGGER_DURATION_MSEG   1
#define MAX_RANGE_CM            30
#define MIN_RANGE_CM            2

bool isInRange(const float distance, const float rangeMin, const float rangeMax);
void us1callback (float distance);
void us2callback (float distance);
void us3callback (float distance);
void us4callback (float distance);

#if US
SR04Data us1_data = {
    .callback = us1callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = 1,
    .trigger_pin = 14,

    .echo_port = 1,
    .echo_pin = 18,
};

SR04Data us2_data = {
    .callback = us2callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = 1,
    .trigger_pin = 15,

    .echo_port = 1,
    .echo_pin = 19,
};

SR04Data us3_data = {
    .callback = us3callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = 1,
    .trigger_pin = 16,

    .echo_port = 1,
    .echo_pin = 26,
};

SR04Data us4_data = {
    .callback = us4callback,
    .unit = METRIC_CM,
    
    .trigger_duration_mseg = TRIGGER_DURATION_MSEG,
    .trigger_port = 1,
    .trigger_pin = 17,

    .echo_port = 1,
    .echo_pin = 27,
};
#endif

#endif    // End _MID_US_H_
