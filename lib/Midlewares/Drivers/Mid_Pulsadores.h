/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _MID_PULSADORES_H_
#define _MID_PULSADORES_H_
#include <Pulsadores.h>

#define FLAG_SWITCH_RELEASE     1
/**************************
    Switch Functionality
***************************/
void pbEvent(void);
#if PULSADOR
const Pulsador_Params pp = {
    .port          =       1,
    .pin           =       9,
    .onPress       = NULL,
    .onRelease     = pbEvent,
    .sampling_time =       1,
    .min_samples   =      20,
};
#endif
#endif    // End _MID_PULSADORES_H_
