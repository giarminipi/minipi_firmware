/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _MID_MOTORES_H_
#define _MID_MOTORES_H_
#include <Motor.h>

#define MOTOR_FREQ_HZ 20000

void SEL_COM (short COM,short Parametro);
#if MOTORES

#endif
#endif    // End _MID_MOTORES_H_
