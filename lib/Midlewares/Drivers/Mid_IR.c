/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/
#include "Mid_IR.h"
#include "GPIO_LPC17xx.h"

extern uint8_t global_input2;

void ir1_rise (void){
//    ledOn(LED_1);
    global_input2 |= 0x01; 
}

void ir2_rise (void){
//    ledOn(LED_3);
    global_input2 |= 0x02; 
}

void ir3_rise (void){
//    ledOn(LED_4);
    global_input2 |= 0x04; 
}

void ir4_rise (void){
//    ledOn(LED_2);
    global_input2 |= 0x08;
}

void ir1_fall (void){
//    ledOff(LED_1);
    global_input2 &= 0xFE;
}

void ir2_fall (void){
//    ledOff(LED_3);
    global_input2 &= 0xFD;
}

void ir3_fall (void){
//    ledOff(LED_4);
    global_input2 &= 0xFB;
}

void ir4_fall (void){
//    ledOff(LED_2);
    global_input2 &= 0xF7;
}
