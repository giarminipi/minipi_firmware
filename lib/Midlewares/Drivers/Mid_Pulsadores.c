/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/
#include <cmsis_os2.h>
#include "Mid_Pulsadores.h"


osThreadId_t mde_tsk_id;
void pbEvent(void)
{
    osThreadFlagsSet(mde_tsk_id, FLAG_SWITCH_RELEASE);
}
