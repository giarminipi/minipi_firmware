/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

#ifndef _MID_MOTORES_H_
#define _MID_MOTORES_H_
#include <as5045.h>
#define FLAG 0x1


void as5045_callback (void);
void test_as5045_task( void *argument );
void SEL_COM (short COM,short Parametro);
#if AS5045

#endif
#endif    // End _MID_MOTORES_H_
