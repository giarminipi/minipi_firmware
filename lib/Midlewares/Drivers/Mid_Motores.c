/**
  ******************************************************************************
  * @file    in_line.c
  * @author  Alejandro Gastón Alvarez
  * @version 1.1
  * @date    12/11/2018
  * @brief   Funciones para el control de sensores infrarrojos
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/
#include <cmsis_os2.h>
#include "Mid_Motores.h"
/**
 * @brief Comando Tabulado
 * @details Actualiza x Comando Tabulado de motores para Maquina de Estados
 * @param COM: Número de Comando
 * @param Parametro: Modificador del Comando
 */
void SEL_COM (short COM,short Parametro){
	switch(COM)
		{
		case 1:
				// Setea PWM1 y PWM2 para ir para ADELANTE
                Motor_SetSpeed(MOTOR1, Parametro);
                Motor_SetSpeed(MOTOR2, Parametro);
				break;
		case 2:
				// Setea PWM1 y PWM2 para girar a la Izq
                Motor_SetSpeed(MOTOR1, Parametro*(-1));
                Motor_SetSpeed(MOTOR2, Parametro);
				break;
		case 3:
				// Setea PWM1 y PWM2 para girar a la Der
                Motor_SetSpeed(MOTOR1, Parametro);
                Motor_SetSpeed(MOTOR2, Parametro*(-1));
				break;
		case 4:
				// Setea PWM1 y PWM2 para RETROCEDER
				Motor_SetSpeed(MOTOR1, Parametro*(-1));
                Motor_SetSpeed(MOTOR2, Parametro*(-1));        
				break;

		default:
			// error: comando no soportado 
				break;
		}
}
