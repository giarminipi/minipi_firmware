/**
 * *****************************************************************************
 * @file    ring_buffer_os.c
 * @author  Alejandro Alvarez
 * @version V1.0.0
 * @date    30-Abril-2016
 * @brief   Glue file for Operative System
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */


#include <ring_buffer_os.h>


/****************************  Mutex Management ********************************/
/**
 * @brief  Create and Initialize a Mutex object
 * @param  mutex_def     mutex definition referenced with \ref osMutex.
 * @retval  mutex ID for reference by other functions or NULL in case of error.
 * @note   MUST REMAIN UNCHANGED: \b osMutexCreate shall be consistent in every CMSIS-RTOS.
 */
int mtx_init ( mtx_t* mutex, int type )
{
    return 0;
}

/**
 * @brief Wait until a Mutex becomes available
 * @param mutex_id      mutex ID obtained by \ref osMutexCreate.
 * @param millisec      timeout value or 0 in case of no time-out.
 * @retval  status code that indicates the execution status of the function.
 * @note   MUST REMAIN UNCHANGED: \b osMutexWait shall be consistent in every CMSIS-RTOS.
 */
int mtx_lock ( mtx_t* mutex )
{
    return 0;
}

/**
 * @brief Release a Mutex that was obtained by \ref osMutexWait
 * @param mutex_id      mutex ID obtained by \ref osMutexCreate.
 * @retval  status code that indicates the execution status of the function.
 * @note   MUST REMAIN UNCHANGED: \b osMutexRelease shall be consistent in every CMSIS-RTOS.
 */
int mtx_unlock ( mtx_t* mutex )
{
    return 0;
}

/**
 * @brief Delete a Mutex
 * @param mutex_id  mutex ID obtained by \ref osMutexCreate.
 * @retval  status code that indicates the execution status of the function.
 * @note   MUST REMAIN UNCHANGED: \b osMutexDelete shall be consistent in every CMSIS-RTOS.
 */
int mtx_destroy ( mtx_t* mutex )
{
    return 0;
}
