/**
  ******************************************************************************
  * @file    ring_buffer.h
  * @author  Alejandro Alvarez
  * @version V1.0.0
  * @date    19-Mayo-2016
  * @brief   Audio Functions for ASR_DTW
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

#ifndef __STDC_NO_THREADS__
#include <ring_buffer_os.h>
#else
#include <threads.h>
#endif


#ifdef __cplusplus
extern "C" {
#endif

    typedef void ( *callback ) ( void );

    typedef struct RingBufferClient {
        uint8_t *ptr;                   /**< Client's pointer inside the buffer */
        uint32_t count;                 /**< Client's data left to read */
        uint32_t client_id;              /**< Client's ID number */
        uint32_t read_size;             /**< Client's prestablished read size (in bytes) */
        uint32_t shift_size;            /**< Client's prestablished shift size (in bytes) */
        bool overrun;                   /**< Flag indicating whether the client was overruned */
        callback client_func;            /**< Client's callback function */
    } RingBufferClient;

    typedef struct RingBuffer {
        uint8_t *buff;                  /**< Buffer's pointer */
        uint32_t buff_size;             /**< Buffer's size in bytes' */
        uint8_t *head;                  /**< Head pointer or Write pointer */
        uint32_t count;                 /**< Amount of valid data in buffer */
        bool can_override;              /**< Flag indicating whether Head can override a Client */

        mtx_t rw_mutex;                 /**< Mutex used for OS sincronization */

        RingBufferClient *clients;      /**< Pointer to list of clients */
        uint32_t num_clients;           /**< Number of clients in the list */
        uint32_t last_given_client_id;  /**< Last given client ID (it always grows) */
    } RingBuffer;


    typedef enum {
        BUFF_OK = 0,
        BUFF_EMPTY,
        BUFF_FULL,
        BUFF_NOT_ENOUGH_SPACE,
        BUFF_NOT_READY,
        BUFF_NO_CLIENTS,
        BUFF_NOT_A_CLIENT,
        BUFF_ERROR,
        BUFF_WAITING_FOR_CLIENTS,
    } RingBufferStatus;


// Public Functions ---------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @brief Initialize the ring buffer
     * This function initialize a given RingBuffer structure.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] buff_size Size in bytes of the buffer
     * @param [in] can_override Flag informig that the head can override
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_init ( RingBuffer* buffer, const size_t buffer_size, bool can_override );
    /**
     * @brief Deinitialize the given ring buffer
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_deinit ( RingBuffer *buffer );
    /**
     * @brief Flushes the given ring buffer
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_flush ( RingBuffer *buffer );

    /**
     * @brief Registers a client in the given buffer
     * This function registers a client to the given ring buffer. It must be used in order to read the buffer.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] read_size Read size to set when ringBuf_read_fixed() is used
     * @param [in] shift_size Read size to set when ringBuf_read_fixed is used
     * @param [in] client_func When not null, the ring buffer will use call this client's function to inform that it can read <read_size> bytes from the buffer
     * @param [out] client_num Returned client number. One must not loose this number
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_registClient ( RingBuffer* buffer, size_t read_size, size_t shift_size, callback client_func, uint32_t* client_id );
    /**
     * @brief Unregisters a client from the given buffer
     * This function unregisters a client from the given buffer. Once the client is unregister it cannot longer read data from the buffer.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [out] client_num Returned client number. One must not loose this number
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_unregistClient ( RingBuffer *buffer, const uint8_t client_num );

    /**
     * @brief Writes data to the given buffer
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] data Pointer to the data array
     * @param [in] write_size Size in bytes of data array
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_write ( RingBuffer* buffer, const uint8_t* data, const size_t write_size );
    /**
     * @brief Reads the preset data size from the given buffer, starting where it left
     * Reads the amount of bytes setted by <read_size> argument in <ringBuf_registClient>. The data is given through <output>, user must not initialized this memory.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [out] output Pointer where data is recorded. Vector's memory it's initialized inside the function
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_read_fix ( RingBuffer *buffer, uint32_t client_id, uint8_t *output );
    /**
     * @brief Reads all the data left in the buffer
     * Reads the amount of bytes setted by <read_size> argument in <ringBuf_registClient>. The data is given through <output>, user must not initialized this memory.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [out] output Pointer where data is recorded. Vector's memory it's initialized inside the function
     * @param [out] read_size Size in bytes of the <output> vector
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_read_all ( RingBuffer *buffer, uint32_t client_id, uint8_t *output, size_t *read_size );
    /**
     * @brief Reads the amount of data indicated by <read_size> argument
     * Reads the amount of bytes passed to <read_size> argument. The data is given through <output>, user must not initialized this memory.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [in] read_size Amount of bytes to read
     * @param [in] shift_size Amount of bytes to shift after read
     * @param [out] output Pointer where data is recorded. Vector's memory it's initialized inside the function
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_read_var ( RingBuffer *buffer, uint32_t client_id, size_t read_size, size_t shift_size, uint8_t *output );
    /**
     * @brief Shifts a client by a given amount of bytes indicated by <shift_size>
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [in] shift_size Amount of bytes to shift the client
     * @return RingBufferStatus Status value of the RingBuffer
     */
    RingBufferStatus ringBuf_shift_Client ( RingBuffer* buffer, const uint32_t client_id, size_t shift_size );

    /**
     * @brief Checks if the given buffer is initialized
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @return bool True if the buffer is initialized
     */
    bool is_ringBuf_init ( RingBuffer *buffer );
    /**
     * @brief Checks if the given buffer is empty
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @return bool True if the buffer is empty
     */
    bool is_ringBuf_empty ( RingBuffer *buffer );
    /**
     * @brief Checks if the given buffer is full
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @return bool True if the buffer is full
     */
    bool is_ringBuf_full ( RingBuffer *buffer );
    /**
     * @brief Checks if the given buffer has clients
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @return bool True if the buffer has at least one client
     */
    bool has_ringBuf_clients ( RingBuffer *buffer );
    /**
     * @brief Returns how much data is available for the client to read
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @param [in] client_id Client's ID number
     * @return size_t Amount of data in bytes that can be read by the client
     */
    size_t ringBuf_available_data ( RingBuffer *buffer, uint32_t client_id );



// Private Functions ---------------------------------------------------------------------------------------------------------------------------------------
    /**
     * @brief Updates the ring buffer valid data
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialized
     * @return void
     */
    static void ringBuf_updateCount ( RingBuffer *buffer );
    /**
     * @brief Reads the amount of data indicated by <read_size> argument and then shifts the pointer by <shift_size>
     * This function should not be used by the user. Reads the amount of bytes passed to <read_size> argument and then shifts the pointer by <shift_size>. The data is given through <output>, and it is initialized insed the funciton.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [in] read_size Amount of bytes to read
     * @param [in] shift_size Amount of bytes to shift after read
     * @param [out] output Pointer where data is recorded. Vector's memory it's initialized inside the function
     * @return RingBufferStatus Status value of the RingBuffer
     */
    static RingBufferStatus ringBuf_read ( RingBuffer* buffer, uint8_t** ptr, uint32_t* count, size_t read_size, size_t shift_size, uint8_t* output );
    /**
     * @brief Finds a client in the list.
     *
     * @param [in] buffer Pointer to the RingBuffer structure to initialize
     * @param [in] client_id Client's ID number
     * @param [out] idx Index from the list for that client
     * @return RingBufferStatus Status value of the RingBuffer
     */
    static RingBufferStatus ringBuf_findClient ( RingBuffer *buffer, const uint32_t client_id, uint32_t *idx );


#ifdef __cplusplus
}
#endif


#endif    // RING_BUFFER_H
