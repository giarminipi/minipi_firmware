#!/usr/bin/env bash
WORKING_DIR=`pwd`
HOOKS_DIR="hooks"
GIT_ROOT=`git rev-parse --show-cdup`

# Loop through all the hook files
for file in `ls ${HOOKS_DIR}`; do
  
  # Generate fullpath for the file
  fullpath="${WORKING_DIR}/${GIT_ROOT%%/}/.git/hooks/${file}"
  
  # If it already exist delete it
  if [ -L "${fullpath}" ]; then
    rm -f "${fullpath}"
  fi

  # Soft link file in hooks folder to .git/hooks folder
  ln -s "${WORKING_DIR}/${HOOKS_DIR}/${file}" "${fullpath}"

  echo "${file} hook installed"
done