#!/bin/bash
for file in `find . -name "*.[hc]"`
do
    file --mime-encoding $file
    iconv -f `file -b --mime-encoding $file` -t utf-8 -o "$file.new" "$file"
    mv -f "$file.new" "$file"
done