.. MiniPi-Firmware documentation master file, created by
   sphinx-quickstart on Sat Apr 11 21:07:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MiniPi-Firmware's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

 
API
====
 
.. doxygenindex::
