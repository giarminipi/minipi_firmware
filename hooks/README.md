# Hooks for git-flow AVH

This hooks are taken from [git-flow-hooks](https://github.com/petervanderdoes/git-flow-hooks) and adapted for our requirements. It also has some standard git hooks that were originally created by [Sitebase](https://github.com/Sitebase/git-hooks).

These hooks are used by me for development of git-flow AVH Edition itself
 
## Installation
For adding the hooks simply run the following script

```shell
./install_hooks.sh
```
It creates a softlink from every file and folders inside the `hooks` folder to the `.git/hooks` directory and deletes any previous hooks with the same filename.

## Requirements
* git-flow AVH edition
* Bash shell script.
* Use the [Semantic Versioning 2.0.0](http://semver.org/) specification.


## Added value
* When you start a _release_ you can omit the version number. The filter will grep 
  the version set in the stable info of the master branch and increase the patch level.
* You *can't commit* on a master branch.
* You *can't commit* files with merge markers.
* Commit messages shorter than 8 are NOT allowed.